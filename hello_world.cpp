// mylib
#include "mylibrary/functions.h"
#include "mylibrary/list.h"

// stl
#include <iostream>
#include <stdexcept>

// The one and only "allowed" MACRO -- MACROs are NO NO NO NOOOOOOOOOOOOO !!!! 
// NOT EVEN SLIGHTLY TYPE SAFE - JUST LOOK AT WHAT THIS DOES (... OR DOES NOT)
#define UNUSED(ARG) (void)ARG;

int main(int argc, char** argv) try {
  UNUSED(argc)
  UNUSED(argv)
  //mylib::helloWorld( "List:" );

  //For testing
  mylib::List list{4,5,6}; //declaring new list

  std::cout << "Is the list empty? Answer: ";
  if (list.isEmpty() != 1)
  {
    std::cout << "List is not empty.\n" << std::endl;
    std::cout<<"List size = "<<list.size()<< "\n"; //function list.size()
    list.display();
  }
  else std::cout << "List is empty.\n" << std::endl;

  std::cout << "\nAdding values to the front and back positions of list." << std::endl;
  list.Push_Back(10);
  list.Push_Front(5);
  std::cout<<"List size = "<<list.size()<< "\n"; //function list.size()
  list.display();

  std::shared_ptr <mylib::Item> first_elem = list.front();
  std::cout<<"\nAdress of the first element - "<<first_elem<<"\n";

  std::shared_ptr <mylib::Item> last_elem = list.back();
  std::cout<<"Adress of the last element - "<<last_elem<<"\n";

  int it_first = list.begin();
  int it_last = list.end();
  std::cout<<"\nFirst element at begin() - "<<it_first<<"\n";
  std::cout<<"Last element at end() - "<<it_last<<"\n\n";

  return 0;
}
catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}
