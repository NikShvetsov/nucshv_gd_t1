// mylib
#include "../mylibrary/vector.h"

// stl
#include <chrono>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdexcept>

// The one and only "allowed" MACRO -- MACROs are NO NO NO NOOOOOOOOOOOOO !!!! 
// NOT EVEN SLIGHTLY TYPE SAFE - JUST LOOK AT WHAT THIS DOES (... OR DOES NOT)
#define UNUSED(ARG) (void)ARG;





///
///
///   This program benchmarks run times when filling our custom vector with data.
///
///

using TimingType = size_t;

template <typename Elem>
using SizeType = typename mylib::Vector<Elem>::size_type;

template <typename Elem>
TimingType
time_fill_no_reserve( typename mylib::Vector<Elem>::size_type n, const Elem& e = Elem() ) {
//  UNUSED(n)
//  UNUSED(e)


  using namespace mylib;

  Vector<Elem> vec{};

  auto start = std::chrono::high_resolution_clock::now();
  for( SizeType<Elem> i {0}; i < n; ++i )
    vec.push_back(e);
  auto end = std::chrono::high_resolution_clock::now();


  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template <typename Elem>
TimingType
time_fill_with_reserve( typename mylib::Vector<Elem>::size_type n, const Elem& e = Elem() ) {


  using namespace mylib;

  Vector<Elem> vec;

  auto start = std::chrono::high_resolution_clock::now();
  vec.reserve(n);
  for( SizeType<Elem> i {0}; i < n; ++i )
    vec.push_back(e);
  auto end = std::chrono::high_resolution_clock::now();


  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template <typename Elem>
TimingType
time_fill_std_with_reserve_and_emplace( typename mylib::Vector<Elem>::size_type n, const Elem& e = Elem() ) {


  using namespace std;

  vector<Elem> vec;

  auto start = std::chrono::high_resolution_clock::now();
  vec.reserve(n);
  for( SizeType<Elem> i {0}; i < n; ++i )
    vec.emplace_back(e);
  auto end = std::chrono::high_resolution_clock::now();


  return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}




template <typename Elem>
struct TimingData {
  SizeType<Elem>    size;
  TimingType        duration;

  TimingData( const SizeType<Elem>& s, const TimingType& d ) 
    : size{s}, duration{d} {}
};


int main(int argc, char** argv) try {
  UNUSED(argc)
  UNUSED(argv)

  using ElementType   = int;

  const ElementType fill_value {0};
  
  std::vector<SizeType<ElementType>> fill_sizes {
    10000,100000
    ,1000000                  // 4MB
    ,10000000
    ,100000000
    ,1000000000               // 4GB (given element type is 4B)
  };

  std::vector<TimingData<ElementType>> timing_data_fill_no_reserve;
  std::vector<TimingData<ElementType>> timing_data_fill_with_reserve;
  std::vector<TimingData<ElementType>> timing_data_fill_std_with_reserve_and_emplace;
  for( const auto& fill_size : fill_sizes ) {
//    timing_data_fill_no_reserve.push_back( TimingData<ElementType>(fill_size, time_fill_no_reserve( fill_size, fill_value ) ) );
    timing_data_fill_with_reserve.push_back( TimingData<ElementType>(fill_size, time_fill_with_reserve( fill_size, fill_value ) ) );
    timing_data_fill_std_with_reserve_and_emplace.push_back( TimingData<ElementType>(fill_size, time_fill_std_with_reserve_and_emplace( fill_size, fill_value ) ) );
  }
 

  // Dump data to file of the format size in aending order
  // size time
  // s0   t0
  // s1   t1
  // s2   t2

  // Build output data
  std::ofstream ofs;
  ofs.exceptions( std::ofstream::failbit | std::ofstream::badbit );
//
//  ofs.open( "timings_fill_no_reserve.dat", std::ofstream::out | std::ofstream::trunc );
//  ofs << "size time" << std::endl;
//  for( const auto& data : timing_data_fill_no_reserve )
//    ofs << data.size << " " << data.duration << std::endl;
//  ofs.close();

  ofs.open( "timings_fill_with_reserve.dat", std::ofstream::out | std::ofstream::trunc );
  ofs << "size time" << std::endl;
  for( const auto& data : timing_data_fill_with_reserve )
    ofs << data.size << " " << data.duration << std::endl;
  ofs.close();

  ofs.open( "timings_fill_std_with_reserve_and_emplace.dat", std::ofstream::out | std::ofstream::trunc );
  ofs << "size time" << std::endl;
  for( const auto& data : timing_data_fill_std_with_reserve_and_emplace )
    ofs << data.size << " " << data.duration << std::endl;
  ofs.close();


  return 0;
}
catch (std::ifstream::failure e) {
  std::cerr << "Exception opening/reading/closing file: " << e.what();
}
catch(const std::exception& e){
  std::cerr << "An exception occurred: " << e.what() << std::endl;
}
catch(...) {
  std::cerr << "Unknown exception thrown!" << std::endl;
}
