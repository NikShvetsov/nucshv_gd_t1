#include <gtest/gtest.h>
#include "../list.h"

TEST(Container_List,Size_functionality)
{
  using namespace mylib;

  List list1 { 1,2,3,4 };
  List list2 {};

  EXPECT_EQ(4,list1.size());
  EXPECT_TRUE(list2.isEmpty());

}

TEST (Container_List,Methods_functionality)

{
    using namespace mylib;

    List list1 { 1,2,3,4 };
    List list3 { 1,2,3,4 };
    List list4 { -1,0,3 };
    List list5 {0};

    //EXPECT_EQ(list1, list3);
    EXPECT_EQ(list1.begin(), list3.begin());
    EXPECT_EQ(list1.end(), list3.end());

    list5.Push_Back(3);
    list5.Push_Front(-1);
    //EXPECT_EQ(list4, list5);
    EXPECT_EQ(list4.begin(),list5.begin());
    EXPECT_EQ(list4.end(), list5.end());

}
