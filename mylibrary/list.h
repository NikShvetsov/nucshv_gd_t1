#ifndef LIST_H
#define LIST_H

#include <initializer_list>
#include <memory>
#include <stdexcept>
#include <iostream>

namespace mylib {

struct Item
{
    int _value;
    std::shared_ptr<Item> _next;

    Item(int value = 0) : _value(value), _next(nullptr) {}
    //default constructor for Item
};

class List {
public:
    using size_type  = std::size_t;
    using value_type = int;
    using const_iterator = std::shared_ptr<const Item>;

    using iterator = std::shared_ptr<Item>; //for front, back //Item* front();

    List( std::initializer_list<value_type> list );
    List( List&& rvalue ); //move constructor
    List( List& rvalue );//copy constructor

    //function prototypes
    size_type size() const;
    bool isEmpty() const;

    Push_Back (int a);
    Push_Front (int a);
    display();

    iterator front();
    iterator back();

    int begin();
    int end();

    //int begin() const;
    //int end() const;

    ~List(){
        //std::cout << "Destructor called: List is destroyed." << std::endl;
    }

  private:
    size_type _size;

    iterator head;
    iterator last;

  }; // END class List

}  // END namespace mylib

#endif // LIST_H
