#include "list.h"
#include <initializer_list>
#include <iostream>
#include <memory>
namespace mylib {

  List::List(std::initializer_list<value_type> list) //List constructor
      : _size {0} //using _size = 0
  {
      //init of head and tail ptr
      iterator head = nullptr;
      iterator last = nullptr;
      //initialize list
      for (auto a:list)  //for each a from list
      {
          //use Push_Back to form a list
          Push_Back(a);
          //or use below with _size = list.size
          /*
          std::shared_ptr<Item> temp = std::make_shared<Item>(a);

          if (head==nullptr)
          {
              //std::shared_ptr<int> foo2 (new int(10));
              //std::shared_ptr<Item> temp;

              //Item * temp = new Item();
              //temp->_value = a;
              //temp->_next = nullptr;

              head=temp;
              last=temp;
          }
          else
          {
              //std::shared_ptr<Item> temp;

              //Item * temp = new Item();
              //temp->value = a;
              //temp->next = nullptr;
              last->_next = temp;
              last = temp;
          }*/
      }
  }

  List::Push_Back(int a) //adding element to list (to ++last position)
  {
     //iterator current = head;
     std::shared_ptr<Item> temp = std::make_shared<Item>(a);

     if(head==nullptr)
       {
         //std::cout<<"\nEmpty List\n";
         head = temp;
         last = temp;
       }
     else
     {
         last->_next = temp;
         last=temp;
     }
     _size++;
  }

  List::Push_Front(int a) //adding element to list (to first position (with shifting))
  {
     iterator current = head;

     std::shared_ptr<Item> temp = std::make_shared<Item>(a);

     if(current==nullptr)
       {
         std::cout<<"\nEmpty List\n";
         last = temp;
       }
     else
     {
         temp->_next = head;
     }
     head=temp;
     _size++;
  }

  List::display() //function to display the list
  {
      iterator current = head;

      if (current==nullptr)
      {
          std::cout<<"\nEmpty list\n";
      }
      else
      {
          while(current!=nullptr)
          {
              std::cout<<" "<<current->_value;
              current = current->_next;
          }
      }
      std::cout<<"\n";
  }


  std::shared_ptr<Item> List::front() //get head pointer of the list
  {
      iterator current = head;
      //std::cout<<"\nAdress of the first element - "<<current<<"\n";
      return head;
  }

  std::shared_ptr<Item> List::back() //get last pointer of the list
  {
      iterator current = head;

      if (current==nullptr)
      {
          std::cout<<"\nEmpty list\n";
          return nullptr;
      }
      else
      {
          while(current!=nullptr)
          {
              if (current -> _next == nullptr) return current;
              else current = current->_next;
          }
          //std::cout<<"\nAdress of the last element - "<<prev<<"\n";
      }
  }

  int List::begin() //function to get the first element value
  {
      iterator current = std::make_shared<Item>();
      current = head;

      if (current==nullptr)
      {
          return 0;
      }
      else
      {
          return current -> _value;
      }
  }

  int List::end() //function to get last element value
  {
      iterator current = std::make_shared<Item>();
      current = head;

      if (current==nullptr)
      {
          return 0;
      }
      else
      {
          while(current!=nullptr)
          {
              current=current -> _next;
              if (current->_next==nullptr)
              {
                break;
              }
          }
          return current -> _value;
      }
  }

  bool List::isEmpty() const //function - check if list is empty
  {
      if (_size == 0) return 1;
      else return 0;
  }

  List::size_type List::size() const { return _size; } //function to get the size of list
  //type            function            function body

} // END namespace mylib


